import {LitElement, html} from 'lit';

class PersonaForm extends LitElement {

    static get properties() {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }

    constructor() {
        super();
        this.resetFormData();        
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div>
            <form>
                <div class="form-group">
                    <label>Nombre completo</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        .value="${this.person.name}" 
                        placeholder="Nombre completo" 
                        @input="${this.updateName}"
                        ?disabled="${this.editingPerson}"
                    />
                </div>
                <div class="form-group">
                    <label>Perfil</label>
                    <textarea 
                        class="form-control" 
                        .value="${this.person.profile}" 
                        placeholder="Perfil" 
                        rows="5" 
                        @input="${this.updateProfile}">
                    </textarea>
                </div>
                <div class="form-group">
                    <label>Años en la empresa</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        .value="${this.person.yearsInCompany}" 
                        placeholder="Años en la empresa" 
                        @input="${this.updateYearsInCompany}" 
                    />
                </div>
                <button class="btn btn-primary" @click="${this.goBack}"><strong>Cancelar</strong></button>
                <button class="btn btn-success" @click="${this.storePerson}"><strong>Guardar</strong></button>
            </form>
        </div>
        `;
    }

    goBack(e){
        console.log("goBack");
        console.log("Se ha pulsado botón atrás formulario persona");
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("persona-form-close", {}));
        this.resetFormData();
    }

    storePerson(e){
        console.log('storePerson');
        console.log('Se va guardar');
        this.person.photo = {
            src: './img/nuevo.png',
            alt: this.person.name
        };
        console.log(this.person);
        e.preventDefault();
       
        this.dispatchEvent(
            new CustomEvent(
                "persona-form-store",
                {
                    detail: {
                        person: {
                            name: this.person.name,
                            profile: this.person.profile,
                            yearsInCompany: this.person.yearsInCompany,
                            photo: this.person.photo
                        },
                        editingPerson: this.editingPerson
                    }
                }
            )
        );
        this.resetFormData();
    }

    updateName(e){
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor " + e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile(e){
        console.log("updateProfile");
        console.log("Actualizando la propiedad profile con el valor " + e.target.value);
        this.person.profile = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad yearsInCompany con el valor " + e.target.value);
        this.person.yearsInCompany = e.target.value;
    }

    resetFormData(){
        console.log("resetFormData");
        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";
        this.editingPerson = false;
    }
}

customElements.define("persona-form", PersonaForm);