import {LitElement, html} from 'lit';

class PersonaMainDm extends LitElement {

    static get properties() {
        return {
            people: {type:Array}
        };
    }

    constructor() {
        super();
        this.people = this.getPeople();        
    }

    updated(changedProperties){
        if (changedProperties.has('people')){
            this.dispatchEvent(
                new CustomEvent(
                    "updated-people",
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            );
        }    
    }

    getPeople(){
        return [
            {
                name: "Luka Modric",
                yearsInCompany: 10,
                photo: {
                    src: "./img/modric.png",
                    alt: "Luka Modric"
                },
                profile: "Mediapunta de gran calidad"
            },
            {
                name: "Karim Benzema",
                yearsInCompany: 15,
                photo: {
                    src: "./img/benzema.png",
                    alt: "Karim Benzema"
                },
                profile: "Delantero polivalente"
            },
            {
                name: "Vinicius JR.",
                yearsInCompany: 4,
                photo: {
                    src: "./img/vini.png",
                    alt: "Vinicius JR."
                },
                profile: "Extremo izquierdo"
            },
            {
                name: "David Alaba",
                yearsInCompany: 1,
                photo: {
                    src: "./img/alaba.png",
                    alt: "David Alaba"
                },
                profile: "Defensa y lateral"
            },
            {
                name: "Eder Militao",
                yearsInCompany: 3,
                photo: {
                    src: "./img/militao.png",
                    alt: "Eder Militao"
                },
                profile: "Defensa central"
            },
            {
                name: "Carlos Casemiro",
                yearsInCompany: 12,
                photo: {
                    src: "./img/casemiro.png",
                    alt: "Carlos Casemiro"
                },
                profile: "Mediocentro defensivo"
            }
        ];       
    }
}

customElements.define("persona-main-dm", PersonaMainDm);