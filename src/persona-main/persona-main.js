import {LitElement, html} from 'lit';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor() {
        super();
        this.showPersonForm = false;
        this.people = [];        
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>            
            <div class="row" id="peopleList">
                <main class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`<persona-ficha-listado fname="${person.name}"
                                        yearsInCompany="${person.yearsInCompany}"
                                        .photo="${person.photo}"
                                        profile="${person.profile}"
                                        @delete-person="${this.deletePerson}"
                                        @info-person="${this.infoPerson}"></persona-ficha-listado>`
                    )}                
                </main>
            </div>
            <div class="row">
                <persona-form id="personForm" class="d-none border rounded border-primary"
                @persona-form-close="${this.personFormClose}"
                @persona-form-store="${this.personaFormStore}"></persona-form>
            </div>
        `;
    }

    updated(changedProperties){
        console.log("updated en persona-main");

        if (changedProperties.has("showPersonForm")){
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en el persona-main");
            
            this.dispatchEvent(
                new CustomEvent(
                    "people-updated",
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            );
        }    

    }

    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);
        
        this.people = this.people.filter(
            person => person.name !== e.detail.name
        );
    }

    infoPerson(e){
        console.log("infoPerson en persona-main");
        console.log("Se ha pedido más información de la persona " + e.detail.name);
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;
        console.log(person);

        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }

    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando el listado de personas");

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showPersonFormData(){
        console.log("showPersonFormData");
        console.log("Mostrando el formulario de personas");

        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    }

    personFormClose(e){
        console.log("personFormClose");
        this.showPersonForm = false;
    }

    personaFormStore(e){
        console.log("personaFormStore");
        console.log(e.detail.person);

        if (e.detail.editingPerson === true) {
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name);
            /*let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );

            if (indexOfPerson >= 0) {
                console.log("Persona encontrada");
                this.people[indexOfPerson].name = e.detail.person.name;
                this.people[indexOfPerson].profile = e.detail.person.profile;
                this.people[indexOfPerson].yearsInCompany = e.detail.person.yearsInCompany;
            }*/

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                ? (e.detail.person.photo.src = person.photo.src,
                    e.detail.person.photo.alt = person.photo.alt, 
                    person = e.detail.person) 
                : person
            );

        } else {
            console.log("Se va a almacenar una persona nueva");
            //this.people.push(e.detail.person);
            this.people = [...this.people, e.detail.person];
        }     
        console.log("Proceso terminado");
        this.showPersonForm = false;
    }
}

customElements.define("persona-main", PersonaMain);