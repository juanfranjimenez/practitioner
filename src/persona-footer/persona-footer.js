import {LitElement, html} from 'lit';

class PersonaFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render(){
        return html`
            <h5>@PersonaApp</h5>
        `;

    }
}

customElements.define("persona-footer", PersonaFooter);